import 'package:flutter/cupertino.dart';
import 'package:moviedex/model/movie_response.dart';
import 'package:moviedex/model/person_response.dart';
import 'package:moviedex/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class SearchPersonBloc {
  final MovieRepository _repository = MovieRepository();
  BehaviorSubject<PersonResponse> _subject = BehaviorSubject<PersonResponse>();

  searchPerson(String personName) async {
    PersonResponse response = await _repository.searchPerson(personName);
    _subject.sink.add(response);
  }

  void drainStream(){ _subject.value = null; }
  @mustCallSuper
  void dispose() async{
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<PersonResponse> get subject => _subject;
}
final searchPersonBloc = SearchPersonBloc();