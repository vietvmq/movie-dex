class PersonDetail {
  final int id;
  final String name;
  final String birthday;
  final String deathday;
  final String known;
  final int gender;
  final String biography;
  final String place;
  final String profileImg;

  PersonDetail(this.id,
      this.name,
      this.birthday,
      this.deathday,
      this.known,
      this.gender,
      this.biography,
      this.place,
      this.profileImg);

  PersonDetail.fromJson(Map<String, dynamic> json)
    : id = json["id"],
      name = json["name"],
      birthday = json["birthday"] ?? null,
      deathday = json["deathday"] ?? null,
      known = json["known_for_department"],
      gender = json["gender"],
      biography = json["biography"] ?? null,
      place = json["place_of_birth"] ?? null,
      profileImg = json["profile_path"] ?? null;
}
